# TODO multi arch images
# FROM ubuntu as buildah

# RUN apt-get -y update
# RUN apt-get -y install buildah

FROM ubuntu as base

ARG IMAGE_BUILD_VERSION=2.5.5-1ubuntu3.1

ENV DEBIAN_FRONTEND=noninteractive
ENV IMAGE_BUILD_VERSION $IMAGE_BUILD_VERSION

RUN apt-get update && apt-get install -y openvpn=$IMAGE_BUILD_VERSION unzip

CMD [ "openvpn", "--config", "/etc/openvpn/ovpn.conf", "--daemon" ]
